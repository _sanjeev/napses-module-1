const express = require('express');
const { sequelize, User, Post } = require('./models');
const { v4: uuidv4 } = require('uuid');
const app = express();
app.use(express.json());

app.post('/users', async (req, res) => {
    const { name, email, number } = req.body;
    const id = uuidv4();
    try {
        const user = await User.create({ id, name, email, number });
        return res.json(user);
    } catch (error) {
        console.log(error);
        return res.status(404).json(error);
    }
});

app.get('/users', async (req, res) => {
    try {
        const users = await User.findAll({
            include: [Post]
        });
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.status(404).json(error);
    }
});

app.get('/users/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    try {
        const user = await User.findOne({
            where: {
                id: uuid,
            }
        });
        return res.json(user);
    } catch (error) {
        return res.status(404).json(error);
    }
})

app.delete ('/users/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    try {
        const user = await User.findOne ({
            where : {
                id: uuid
            }
        });
        await user.destroy();
        return res.json ({
            message: "User successfully deleted"
        })
    } catch (error) {
        return res.json (error);
    }
});

app.put ('/users/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    const {name, email, number} = req.body;

    try {
        const user = await User.findOne ({
            where: {
                id: uuid,
            }
        });

        user.name = name;
        user.email = email;
        user.number = number;

        await user.save();

        return res.json(user);

    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.post('/posts', async (req, res) => {
    const { uuid, body, name, email, number } = req.body;
    try {
        let user = await User.findOne({
            where: {
                id: uuid
            }
        });
        if (user === null) {
            const id = uuidv4();
            user = await User.create({ id, name, email, number });
        }
        const postId = uuidv4();
        const posts = await Post.create({
            id: postId, body, userId: user.id
        });
        return res.json(posts);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.get ('/posts', async (req, res) => {
    try {
        const posts = await Post.findAll ({
            include: [User]
        });
        return res.json (posts);
    } catch (error) {
        return res.status (404).json (error);
    }
})

app.listen({ port: 3000 }, async () => {
    console.log('Server hosted on http://localhost:3000');
    await sequelize.authenticate();
    console.log('Database Synced');
})

